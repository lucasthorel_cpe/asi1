package com.sp.model;

public class CardFormDTO {
	private String familysrc;
	private String familyname;
	private String imgsrc;
	private String name;
	private String description;
	private String hp;
	private String energy;
	private String attack;
	private String defense;
	private String affinity;
	private String price;
	
	public String getFamilysrc() {
		return familysrc;
	}
	public void setFamilysrc(String family_src) {
		this.familysrc = family_src;
	} 
	
	
	public String getFamilyname() {
		return familyname;
	}
	public void setFamilyname(String family_name) {
		this.familyname = family_name;
	}
	
	
	public String getImgsrc() {
		return imgsrc;
	}
	public void setImgsrc(String imgSrc) {
		this.imgsrc = imgSrc;
	}    
	
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
    
	
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
    
	
	public String getHp() {
		return hp;
	}
	public void setHp(String hp) {
		this.hp = hp;
	}
    
	
	public String getEnergy() {
		return energy;
	}
	public void setEnergy(String energy) {
		this.energy = energy;
	}
    
	
	public String getAttack() {
		return attack;
	}
	public void setAttack(String attack) {
		this.attack = attack;
	}
    
	
	public String getDefense() {
		return defense;
	}
	public void setDefense(String defense) {
		this.defense = defense;
	}
	
	
	public String getAffinity() {
		return affinity;
	}
	public void setAffinity(String affinity) {
		this.affinity = affinity;
	} 
	
	
	public String getPrice() {
		return price;
	}
	public void setPrice(String price) {
		this.price = price;
	} 

	public CardFormDTO() {
		this.familysrc = "";
		this.familyname = "";
		this.imgsrc = "";
		this.name="";
		this.description="";
		this.hp="";
		this.energy="";
		this.attack="";
		this.defense="";
		this.price = "";
	}
	
	public CardFormDTO(String family_src,String family_name,String img_src, String name, String description, String hp, String energy, String attack, String defense,String affinity, String price) {
		this.familysrc = family_src;
		this.familyname = family_name;
		this.imgsrc = img_src;
		this.name=name;
		this.description=description;
		this.hp=hp;
		this.energy=energy;
		this.attack=attack;
		this.defense=defense;
		this.affinity = affinity;
		this.price = price;
	}
	
	
	public String toJson() {
		
		return "{"
			   +" \"name\": \""+this.name+"\","
			   +" \"description\": \""+this.description+"\","
			   +" \"family\": \""+this.familyname+"\","
			   +" \"affinity\": \""+this.affinity+"\","
			   +" \"imgUrl\": \""+this.imgsrc+"\","
			   +" \"smallImgUrl\": \""+this.imgsrc+"\","
			   +" \"energy\": "+this.energy+","
			   +" \"hpv\": "+this.hp+","
			   +" \"defence\": "+this.defense+","
			   +" \"attack\": "+this.attack+","
			   +" \"price\": "+this.price+""
			   +"}";
		
	}

}
