package com.sp.controller;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
public abstract class HTTPRequest {
    public static void execdata(String data, String URL) throws Exception {
        String payload = data;
        StringEntity entity = new StringEntity(payload,
                ContentType.APPLICATION_JSON);

        HttpClient httpClient = HttpClientBuilder.create().build();
        HttpPost request = new HttpPost(URL);
        request.setEntity(entity);

        HttpResponse response = httpClient.execute(request);
        System.out.println("ici ->" +response.getStatusLine().getStatusCode());
    }
    
}