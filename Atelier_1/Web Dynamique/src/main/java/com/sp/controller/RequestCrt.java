  package com.sp.controller;

  import org.springframework.beans.factory.annotation.Autowired;
  import org.springframework.beans.factory.annotation.Value;
  import org.springframework.stereotype.Controller;
  import org.springframework.ui.Model;
  import org.springframework.web.bind.annotation.ModelAttribute;
  import org.springframework.web.bind.annotation.RequestMapping;
  import org.springframework.web.bind.annotation.RequestMethod;
  import org.springframework.web.bind.annotation.PathVariable;

  import com.sp.model.CardFormDTO;
  
  @Controller // AND NOT @RestController
  public class RequestCrt {
  
  	@RequestMapping(value = { "/", "/card" }, method = RequestMethod.GET)
  	public String card(Model model) {
    
  		model.addAttribute("family_src", "https://upload.wikimedia.org/wikipedia/commons/thumb/1/1c/DC_Comics_logo.png/280px-DC_Comics_logo.png");
  		model.addAttribute("family_name", "DC comics");
  		model.addAttribute("img_src", "http://www.superherobroadband.com/app/themes/superhero/assets/img/superhero.gif");
  		model.addAttribute("name", "SUPERMAN");
  		model.addAttribute("description", "The origin story of Superman relates that he was born Kal-El on the planet Krypton, before being rocketed to Earth as an infant by his scientist father Jor-El, moments before Krypton's destruction. Discovered and adopted by a farm couple from Kansas, the child is raised as Clark Kent and imbued with a strong moral compass. Early in his childhood, he displays various superhuman abilities, which, upon reaching maturity, he resolves to use for the benefit of humanity through a 'Superman' identity.");
  		model.addAttribute("hp", "50 HP");
  		model.addAttribute("energy", "100 Energy");
  		model.addAttribute("attack", "17 Attack");
  		model.addAttribute("defense", "80 defence");
  		return "searchCard";
  	}
  	
  	@RequestMapping(value = { "/", "/addCard" }, method = RequestMethod.GET)
  	public String addcardform(Model model) {
  		CardFormDTO cardForm = new CardFormDTO();
  		model.addAttribute("cardForm", cardForm);

    
  		return "addCard";
  	}  	
  	@RequestMapping(value = { "/", "/addCard" }, method = RequestMethod.POST)
  	public String addcard(Model model, @ModelAttribute("cardForm") CardFormDTO CardFormDTO) {
  		System.out.println(CardFormDTO.toJson());
  		try {
			HTTPRequest.execdata(CardFormDTO.toJson(), "https://asi2-backend-market.herokuapp.com/card");
			System.out.println("OK");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
  		return "addCard";
  	}
  }
