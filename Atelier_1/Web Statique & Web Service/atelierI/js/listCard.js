$.ajax({
    url: "https://asi2-backend-market.herokuapp.com/cards",
    type: 'GET',
    dataType: 'json',
    success: function(res) {
        let template = document.querySelector("#selectedCard");
        for(const card of res){
            let clone = document.importNode(template.content, true);

            newContent= clone.firstElementChild.innerHTML
            .replace(/{{imgUrl}}/g, card.imgUrl)
            .replace(/{{name}}/g, card.name)
            .replace(/{{description}}/g, card.description)
            .replace(/{{hp}}/g, card.hp)
            .replace(/{{energy}}/g, card.energy)
            .replace(/{{attack}}/g, card.attack)
            .replace(/{{defense}}/g, card.defence);
            clone.firstElementChild.innerHTML= newContent;

            let cardContainer= document.querySelector("#gridContainer");
            cardContainer.appendChild(clone);
        }
    }
})