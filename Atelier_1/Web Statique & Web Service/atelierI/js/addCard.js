function send() {
  var card = {
      name:$("#name").val(),
      description:$("#description").val(),
      imgUrl:$("#imgUrl").val(),
      smallImgUrl:$("#smallImgUrl").val(),
      price:parseInt($("#price").val(),10),
      family:$("#family").val(),
      affinity:$("#affinity").val(),
      hp:parseInt($("#hp").val(),10),
      energy:parseInt($("#energy").val(),10),
      attack:parseInt($("#attack").val(),10),
      defence:parseInt($("#defence").val(),10),
  }
  alert(JSON.stringify(card));

  $.ajax({
      url: 'https://asi2-backend-market.herokuapp.com/card',
      type: "POST",
      dataType: 'json',
      data: JSON.stringify(card),
      contentType: 'application/json;charset=UTF-8',
  })
}