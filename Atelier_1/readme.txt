Les membres du groupes :
	- Tom ECHER
	- Lucas THOREL

Les éléments réalisés du cahier des charges :
	- Utilisation de Web Statique en interaction avec un Web Service existant via javascript et AJAX.
		/addCard.html pour ajouter une carte
		/listCard.html pour lister les cartes
	- Utilisation du Web Dynamique en utilisant la technologie Springboot et tout particulièrement les Thymeleaf et les Services.
		/card pour afficher une carte
		/addCard pour ajouter une carte
	- Expliquer en quoi lesprototypes respectent le pattern MVC, expliquer les avantages et les inconvénients des approches Web Statique + Web Service et Dynamique.

Les éléments non-réalisés du cahier des charges :
	-

Des éléments éventuels réalisés en plus du cahier des charges
	-

Lien dépôt GitLab :
	- https://gitlab.com/lucasthorel_cpe/asi1